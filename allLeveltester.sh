#!/bin/bash
declare -i S=0
declare -i F=0;
declare -i R=0;
for level in {1..4}; do
    inputFile="examples/level_$level/input_lvl${level}_2.txt"
    outputFile="examples/level_$level/output_lvl${level}_2.txt"
    for i in {1..3}; do
        START_TIME=$SECONDS
        java -jar build/jar/Main.jar $inputFile $outputFile
        R="$?"
        runtime=$(($SECONDS - $START_TIME))
        if [ "$R" -eq 3 ];then
            S=$((S+1))
            echo "Level$level,Trial$i: SUCCEEDED in $runtime seconds"
            echo "Level$level,Trial$i: SUCCEEDED in $runtime seconds" >> allleveltestresults.txt
        else
            F=$((F+1))
            echo "Level$level,Trial$i: FAILED in $runtime seconds"
            echo "Level$level,Trial$i: FAILED in $runtime seconds" >> allleveltestresults.txt
        fi
        T=$((T+1))
    done
done
