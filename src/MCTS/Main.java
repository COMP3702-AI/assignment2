package MCTS;

import problem.Action;
import problem.ActionType;
import problem.ProblemSpec;
import simulator.Simulator;
import simulator.State;
import MCTS.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        ProblemSpec ps;
        try {

            String inputFile = args[0];
            String outputFile = args[1];
            //String inputFile = "examples/level_" + level + "/input_lvl" + level + "_2" + ".txt";
            //String outputFile = "examples/level_" + level + "/output_lvl" + level + "_2" + ".txt";

            ps = new ProblemSpec(inputFile);
            Simulator sim = new Simulator(ps, outputFile);
            State state = sim.reset();
            MCTS mcts = new MCTS(ps,sim,state);

            boolean verbose = false;

            if (verbose) {
                System.out.println(ps.toString());

                System.out.println("Finished loading!");

                System.out.println(state.toString());
            }
            Action action;
            Action lastAction = null;
            int stepN = 0;
            while (!sim.isGoalState(state) && sim.getSteps() <= ps.getMaxT()+1) {

                /*if (ps.getLevel().getLevelNumber() > 1 && state.getFuel() < 20) {
                    action = new Action(ActionType.ADD_FUEL, 10);
                } else {
                    action = mcts.SearchForNextAction(state,stepN,lastAction);
                }*/

                action = mcts.SearchForNextAction(state,stepN,lastAction);
                //action = new Action(ActionType.MOVE);

                state = sim.step(action);

                if (state == null){break;}

                if (verbose) {
                    System.out.println("Time Step " + sim.getSteps() + ", Action taken " + action.getText());
                    System.out.println(state.toString());
                }
                stepN++;

                lastAction = action;
            }

            if (sim.isGoalState(state)) {
                //System.out.println("GOOOOOOAAAAAL!!");
                System.exit(3);
            }

            if (sim.getSteps() == ps.getMaxT() || sim.getSteps() > ps.getMaxT()) {
                System.exit(4);
            }
        } catch (IOException e) {
            System.out.println("IO Exception occurred");
            System.exit(1);
        }
        // loop until maxT or goal
        // Register the current location
        // Run MCTS until 15 s
        // Send an action to simulator
        // endloop
    }
}