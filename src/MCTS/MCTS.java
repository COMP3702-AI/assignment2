package MCTS;

import problem.*;
import simulator.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.io.PrintWriter;
import java.io.FileWriter;

/** RE: Still need a way to track T until maxT through the nodes
 *    --> Check out line 64 while loop
 *
 * RE: Also need a way to prune the Rollout phase of a tree and unused branches after actions
 *    --> no need to prune branches.
 *    --> The algorithm does this itself by selecting nodes which give higher rewards over time.
 *
 * RE: IM NOT SURE WHICH PLACES TO UPDATE Ni (node nVisits)
 *  --> check out line 152 - function Backup
**/

/**
 * A class to enable MCTS functionality.
 * Created by Meetkumar Naik on 31/Oct/2018, Boo!.
 */
public class MCTS {
    private static double epsilon = 1e-6;
    private static double discount = 0.95;
    private static final double GOAL_REWARD = 200; //50
    private static final double FORWARD_REWARD = 100;//
    private static final double REVERSE_REWARD = -40; //-20
    private static final double CONSTANT = 70; //exploration 70

    private static long MAX_TIME;
    private static Random random = new Random();
    private List<Action> allPossibleActions;
    private ProblemSpec ps;
    private Simulator sim;
    private Rollout rollout;
    private Node rootNode;
    private int Ni; //total number of sims after last action
    private boolean verbose = false;
    private boolean diagnostics  = false;
    private FileWriter fileWriter;
    private PrintWriter printWriter;
    private int explore;
    private int exploit;

    /**
     * Monte Carlo Tree Search.
     * @param ps
     */
    public MCTS (ProblemSpec ps, Simulator sim, State startState) throws IOException {
         this.ps = ps;
         this.sim = sim;
         this.allPossibleActions = this.getAllPossibleActions();
         this.rollout = new Rollout(this.ps);
         this.rootNode = new Node(startState);

         if (ps.getLevel().getLevelNumber() < 3) {
             MAX_TIME = 5000;
         } else {
             MAX_TIME = 10000;
         }

         //this.fileWriter = new FileWriter("out/diagnostics.txt");
         //this.printWriter = new PrintWriter(fileWriter);
    }

    /**
     * One instance of the online method of Monte Carlo Tree Search.
     *
     * Set the root to the new state given by simulator after the last action.
     * Call selection, expansion, rollout and backup until time is up.
     * Return the best action
     *
     * @param currentState -- initialize root node
     * @return A the action that MCTS finds is best.
     */
    public Action SearchForNextAction (State currentState,int stepN, Action lastAction) throws IOException {

        if (diagnostics) {
            this.fileWriter = new FileWriter("out/diagnostics" + stepN + ".txt");
            this.printWriter = new PrintWriter(fileWriter);
        }


        if (lastAction != null) { // not coming from the start of the game
            for (Node c : rootNode.children) {
                if (c.action == lastAction) {
                    rootNode = c;
                    break;
                }
            }
        } else {
            rootNode = new Node(currentState);
        }

        rootNode = new Node(currentState);

        long startTime = System.currentTimeMillis();

        Ni = 0; //total number of sims
        int loop = 0;
        while ((System.currentTimeMillis() - startTime) < MAX_TIME){ // 15 seconds to find reliable policy

            Node leafNode = Selection(rootNode);

            double reward;

            if (leafNode.nVisits == 0){ //&& !rollout.isGoalState(leafNode.state)) {
                reward = Simulation(leafNode);

                Ni++;
                leafNode.sims++;
                Backup(leafNode,reward);
            } else {


                if (leafNode == rootNode) { //expanding from root
                    Expansion(leafNode, lastAction,stepN);
                    //System.out.println("Children after expansion: " + leafNode.children.size());
                } else { //expanding in subtree
                    Expansion(leafNode, null,1);
                }

                //Expansion(leafNode, lastAction, stepN);

                leafNode = leafNode.children.get(aRandomInt(0,leafNode.children.size()));

                reward = Simulation(leafNode);

                Ni++;
                leafNode.sims++;

                Backup(leafNode,reward);
            }
            if (verbose && loop!=0) {
                for(Node c : rootNode.children) {
                    System.out.print(c.QValue + " ");
                }
                System.out.println("\n");
            }

            loop++;
        }
        if (diagnostics) {
            printWriter.println("----------------------------------------------------------------------------------------");
            printWriter.close();
        }

        //System.out.println(Ni);
        /* return child with the highest QValue reward. exploration term set to 0 */
        return selectOptimalNode(rootNode, 0).action;
    }

    /**
     * Function to conduct Selection phase of MCTS loop starting from root node.
     * Select best nodes until tree is traverse to a leaf node using the UCB1 equation
     * @return best leaf node for expansion
     */
    private Node Selection(Node rootNode){
        Node currentNode = rootNode;
        while (!currentNode.isLeafNode()) {
            if (currentNode == rootNode) {

                currentNode = selectOptimalNode(currentNode, CONSTANT);

                if (verbose) {
                    System.out.println(currentNode.parent.children.indexOf(currentNode));
                }
                if (diagnostics) {
                    printWriter.println(currentNode.parent.children.indexOf(currentNode));
                }
            } else {
                currentNode = selectOptimalNode(currentNode, CONSTANT);
            }

        }
        return currentNode;
    }

    /**
     * Function to conduct Expansion phase of MCTS loop.
     * Leaf node is expanded and all possible actions
     * are children of terminal state
     * @param leafNode to find the possible actions and their resultant states.
     */
    private void Expansion (Node leafNode, Action lastAction, int stepN) {
        leafNode.children = new ArrayList<Node>();

        if (stepN == 0 && lastAction == null) { //coming from the start of the program, include all actions for this rootNode except ones that land you in the same state
            for (Action action : allPossibleActions) {
                StateTimePair explore = rollout.new_state(leafNode.state, action);
                State stateOfChild = explore.state;
                if (action.getActionType() == ActionType.MOVE || !stateOfChild.toString().equals(leafNode.state.toString())) {
                    Node newChild = new Node(leafNode, stateOfChild, action);
                    leafNode.children.add(newChild);
                    newChild.depth = leafNode.depth + explore.timespent;
                }
            }
        } else if (lastAction != null && stepN != 0){// coming from a rootNode but after some action,
            // don't include same action types as you took last time other than A1
            for (Action action : allPossibleActions) {
                StateTimePair explore = rollout.new_state(leafNode.state, action);
                State stateOfChild = explore.state;
                if (action.getActionType() == ActionType.MOVE || (action.getActionType() != lastAction.getActionType() && !stateOfChild.toString().equals(leafNode.state.toString()))) {
                    Node newChild = new Node(leafNode, stateOfChild, action);
                    leafNode.children.add(newChild);
                    newChild.depth = leafNode.depth + explore.timespent;
                }
            }
        } else { // coming from a non root node, dont include action types of the action you took to get to the leaf.
            //System.out.println("3");
            for (Action action : allPossibleActions) {
                StateTimePair explore = rollout.new_state(leafNode.state, action);
                State stateOfChild = explore.state;
                if (action.getActionType() == ActionType.MOVE || (leafNode.action.getActionType() != action.getActionType() && !stateOfChild.toString().equals(leafNode.state.toString()))) {
                    Node newChild = new Node(leafNode, stateOfChild, action);
                    leafNode.children.add(newChild);
                    newChild.depth = leafNode.depth + explore.timespent;
                }
            }
        }
    }

    /**
     * Function to conduct simulation/rollout phase of MCTS loop.
     * Simulate given random action from previously unvisited node
     * @param leafNode random action to undertake simulation from lead node
     */
    public double Simulation(Node leafNode){
        int time = leafNode.depth; //Maybe we can have this as the time that its taken to get to leafNode from root??
        int time0 = time;
        State currentState = leafNode.state;
        leafNode.sims++;
        int runs = 0;
        //while (time <= ps.getMaxT() && !rollout.isGoalState(currentState) && currentState.getFuel() > 5) {
        while (runs < 10 && time <= ps.getMaxT() && !rollout.isGoalState(currentState) && currentState.getFuel() > 5) {
            //Action a = // random action
            //int randomActionID = aRandomInt(1,allPossibleActions.size());
            //Action a = allPossibleActions.get(randomActionID); // action random action
            Action a = new Action(ActionType.MOVE);
            StateTimePair result = rollout.new_state(currentState,a);
            time = time + result.timespent;
            currentState = result.state;
            runs++;
        }

        int N0 = leafNode.state.getPos();
        int N1 = currentState.getPos();
        return Reward(N0, N1, time);
    }

    /**
     * use the value of the terminal state and the tree
     * to traverse up it and update the Qvalue of the nodes and visits
     *
     * @param leafNode where the sim was done, reward that the sim gave
     */
    private void Backup(Node leafNode, double qReward) {
        Node node = leafNode;
        while (node != null) {
            // update and back propagate
            node.nVisits++;
            // the average total discounted reward over the simulations
            // that start from s and performs action a as its first action
            node.QValue = (node.QValue * node.nVisits + qReward) / (node.nVisits + 1);
            node = node.parent;
            qReward = qReward*discount;
        }

    }

    /**
     * Return reward according to position on road of the specified state.
     * Goal State results in very big reward
     * @param N1,N0,timeSteps the distances between leaf and terminal
     * @return double
     */
    private double Reward(int N0, int N1, int timeSteps) {


        if (ps.getLevel().getLevelNumber() < 3) {
            if (N1 == ps.getN()) {
                return 2 * 50;
            } else if ((N1 - N0) > 0) {
                return 50 * (((N1 - N0) + 0.01) / timeSteps);
            } else if ((N1 - N0) < 0) {
                return -20 * 3 * (((N0 - N1) + 0.01) / timeSteps);
            } else { // didnt move
                return -20;
            }
        } else {
            if (N1 == ps.getN()) {
                return 2 * GOAL_REWARD;
            } else if ((N1 - N0) > 0) {
                return FORWARD_REWARD * (((N1 - N0) + 0.01) / timeSteps);
            } else if ((N1 - N0) < 0) {
                return REVERSE_REWARD * 3 * (((N0 - N1) + 0.01) / timeSteps);
            } else { // didnt move
                return REVERSE_REWARD;
            }

        }
    }


    /**
     * Determines the best child node among parent's children
     * @param parent Node
     * @return best child node
     */
    private Node selectOptimalNode(Node parent, double constant) {
        double bestValue = -Double.MAX_VALUE;
        Node bestChild = null;
        for(Node c : parent.children) {
            //Calculate  UCT value => Exploitation + Exploration
            double uct = getUCTValue(c, constant);
            if (uct > bestValue && !rollout.isGoalState(c.state)) {
                bestValue = uct;
                bestChild = c;
            }
        }
        return bestChild;
    }

    /**
     * Calculate  UCT value => Exploitation + Exploration
     * @param node
     * @param  constant
     * @return UCT value
     */
    public double getUCTValue(Node node, double constant) {
        double exploitation = node.QValue;
        double exploration = constant*Math.sqrt(Math.log(node.parent.nVisits + 1) / (node.nVisits + epsilon));
        //double exploration = constant*(Math.sqrt(Math.log(Ni)/(node.sims)));
        double tiebreaker = random.nextDouble() * epsilon; // Random term breaks tie among unexpanded nodes randomly
        return exploitation + exploration + tiebreaker;
    }


    /**
     * List of all possible actions given level
     * @return List<Action>
     */
    private List<Action> getAllPossibleActions () {

        List<Action> allPossibleActions = new ArrayList<>();
        List<ActionType> validActionTypes = ps.getLevel().getAvailableActions();
        List<TirePressure> validPressures = new LinkedList<>();
        validPressures.add(TirePressure.FIFTY_PERCENT);
        validPressures.add(TirePressure.SEVENTY_FIVE_PERCENT);
        validPressures.add(TirePressure.ONE_HUNDRED_PERCENT);
        int fuel = 20;

        for (ActionType actionType : validActionTypes) {
            switch(actionType.getActionNo()) {
                case 1:
                    allPossibleActions.add(new Action(actionType));
                    break;
                case 2:
                    for (String car : ps.getCarOrder()){
                        allPossibleActions.add(new Action(actionType, car));
                    }
                    break;
                case 3:
                    for (String driver : ps.getDriverOrder()) {
                        allPossibleActions.add(new Action(actionType, driver));
                    }
                    break;
                case 4:
                    for (Tire tire : ps.getTireOrder()) {
                        allPossibleActions.add(new Action(actionType, tire));
                    }
                    break;
                case 5:
                    //fuel = ProblemSpec.FUEL_MIN;
                    //while (fuel <= ProblemSpec.FUEL_MAX) {
                        allPossibleActions.add(new Action(actionType, fuel));
                      //  fuel+= 10;
                    //}
                    break;
                case 6:
                    for (TirePressure pres : validPressures) {
                        allPossibleActions.add(new Action(actionType, pres));
                    }
                    break;
                case 7:
                    for (String car : ps.getCarOrder()){
                        for (String driver : ps.getDriverOrder()) {
                            allPossibleActions.add(new Action(actionType, car, driver));
                        }
                    }
                    break;
                case 8:
                    break;
                default:
                    return null; // no possible actions
            }
        }
        return allPossibleActions;
    }

    /** I'm a helper method */
    private static int aRandomInt(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min)) + min;
    }

}


