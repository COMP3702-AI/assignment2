package MCTS;

import problem.*;

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

/**
 * A search tree node for MCTS. Stores
 * a state, a value, the number of times this node has been visited
 * and the action to this state?
 *
 * Created by Meetkumar Naik on 31/Oct/2018, Boo!.
 */

//public class Node implements Comparable<Node> {
public class Node{

    public simulator.State state;
    public Action action;
    public double QValue;
    public Node parent;
    public int nVisits;
    public List<Node> children;
    public int depth;
    public int sims;

    /**
     * Construct a root node of a search tree.
     * @param state the state associated with this
     *                      search tree node.
     */
    public Node(simulator.State state) {
        this.parent = null;
        this.children = null;
        this.state = state;
        this.action = null;
        this.nVisits = 0;
        this.QValue = 0;
        this.depth = 0;
        this.sims = 0;
    }

    /**
     * Construct a non-root search tree node.
     * @param parent the parent of this node
     * @param action taken from parent state to reach Node state
     * @param state the state associated with this
     *                      search tree node.
     */
    public Node(Node parent, simulator.State state, Action action) {
        this.parent = parent;
        this.state = state;
        this.action = action;
        this.children = null;
        this.nVisits = 0;
        this.QValue = 0;
        this.sims = 0;
    }

    /**
     *
     * @return True if leaf node and false if not
     */
    public boolean isLeafNode(){
        /*if (this.children.size() == 0) {
            return true;
        } else {
            return false;
        }*/

        return this.children == null;
    }



    /**
     * Compares the path cost of this node to the path cost of node s.
     *
     * Defining a "compareTo" method is necessary in order for searchTreeNodes
     * to be used in a priority queue.
     *
     * @param s node to compare path cost with
     * @return  -1 if this node has a lower total path cost than node s
     *          0 if this node has the same total path cost as node s
     *          1 if this node has a greater total path cost than node s
     */
    /*public int compareTo(Node s) {
        return Double.compare(this.pathCost, s.pathCost);
    }*/
}
