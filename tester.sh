#!/bin/bash

declare -i S=0
declare -i F=0;
declare -i E=0;
declare -i T=0;
declare -i R=0;

for i in {1..30}; do
    START_TIME=$SECONDS
    java -jar build/jar/Main.jar
    R="$?"
    runtime=$(($SECONDS - $START_TIME))
    if [ "$R" -eq 4 ];then
        F=$((F+1))
        echo "$i. FAIL in $runtime"
        echo "$i. FAIL in $runtime" >> testresults.txt
    else
        S=$((S+1))
        echo "$i. GOAL in $runtime"
        echo "$i. GOAL in $runtime" >> testresults.txt
    fi
    T=$((T+1))
done
echo "Total: $T, Successes: $S, Failures: $F, Errors: $E" >> testresults.txt
echo "Total: $T, Successes: $S, Failures: $F, Errors: $E"

